libtext-findindent-perl (0.11-5) unstable; urgency=medium

  * Team upload.
  * Fix typo in long description.
  * Add patch to make Makefile.PL work without Module::Install::DSL.
    Thanks to Niko Tyni for the bug report. (Closes: #1040021)
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Sat, 01 Jul 2023 17:05:16 +0200

libtext-findindent-perl (0.11-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 14:16:13 +0000

libtext-findindent-perl (0.11-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtext-findindent-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 20:20:29 +0100

libtext-findindent-perl (0.11-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 7 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add missing build dependency on libmodule-install-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 01 Jul 2022 00:26:58 +0100

libtext-findindent-perl (0.11-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:46:15 +0200

libtext-findindent-perl (0.11-1) unstable; urgency=medium

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Angel Abad ]
  * Email change: Angel Abad -> angel@debian.org

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable.
  * Import upstream version 0.11.
    + fixes regex warnings with Perl 5.22. (Closes: #809107)

 -- Niko Tyni <ntyni@debian.org>  Wed, 30 Dec 2015 20:52:43 +0200

libtext-findindent-perl (0.10-1) unstable; urgency=low

  * Team upload.
  * New upstream release
  * debian/copyright: Refer to Debian systems in general instead of only
    Debian GNU/Linux systems.

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 05 Jan 2011 22:26:58 +0100

libtext-findindent-perl (0.09-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Add libtest-simple-perl to B-D-I
    - Add myself to Uploaders
  * debian/copyright:
    - Update copyright years
    - Update license information
  * Bump Standards-Version to 3.9.1 (no changes)
  * Switch to dpkg-source format 3.0 (quilt)

 -- Angel Abad <angelabad@gmail.com>  Wed, 22 Sep 2010 01:05:00 +0200

libtext-findindent-perl (0.08-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 06 Jan 2010 23:50:51 -0500

libtext-findindent-perl (0.07-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 23 Dec 2009 08:46:12 -0500

libtext-findindent-perl (0.05-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Refresh copyright information
  * Standards-Version 3.8.3 (no changes)
  * Use new short debhelper rules format

  [ Ryan Niebur ]
  * New upstream release
  * Update debian/copyright years for inc/*

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * debian/copyright: update years of upstream copyright.

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 26 Nov 2009 19:17:54 -0500

libtext-findindent-perl (0.03-1) unstable; urgency=low

  * Initial Release. (Closes: #511659)

 -- Damyan Ivanov <dmn@debian.org>  Tue, 13 Jan 2009 09:51:54 +0200
