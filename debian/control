Source: libtext-findindent-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           Angel Abad <angel@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-install-perl
Build-Depends-Indep: perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtext-findindent-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtext-findindent-perl.git
Homepage: https://metacpan.org/release/Text-FindIndent
Rules-Requires-Root: no

Package: libtext-findindent-perl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${perl:Depends}
Description: module to heuristically determine indentation style
 Text::FindIndent is a Perl module that attempts to detect the underlying
 indentation "policy" for a text file (most likely a source code file). You
 give it a chunk of text, and it tells you if tabs, spaces or combination of
 both are used for indentation.
 .
 It honours Vim modelines and Emacs Local Variables settings.
